use rodio::source::{SineWave, Source};
use rodio::{OutputStream, Sink};
use std::io::{stdin, stdout, BufRead, Write};
use std::time::Duration;

fn main() {
    let (_stream, stream_handle) = OutputStream::try_default().unwrap();
    let sink = Sink::try_new(&stream_handle).unwrap();
    sink.play();
    sink.set_speed(2.0);
    let stdin = stdin();
    let mut stdout = stdout().lock();
    let mut lines = stdin.lock().lines();
    while let Some(Ok(line)) = lines.next() {
        let base = if line.contains("TRACE") {
            1
        } else if line.contains("DEBUG") {
            2
        } else if line.contains("INFO") {
            4
        } else if line.contains("WARN") {
            6
        } else if line.contains("ERROR") {
            8
        } else {
            3
        };
        let frequency = 100.0 * (base as f32);
        let source = SineWave::new(frequency)
            .take_duration(Duration::from_secs_f32(1.0))
            .amplify(0.10);
        sink.clear();
        sink.append(source.clone());
        sink.play();
        writeln!(&mut stdout, "{}", line).unwrap();
    }
}
